states = ["Kansas", "Nebraska", "North Dakota", "South Dakota"]

# Returns a URL-friendly version of a string.
#   Example: "North Dakota" -> "north-dakota"
def urlify(string)
  string.downcase.split.join('-')
end

def my_urlify(string)
	string = "https://example.com/" + string.downcase.split.join('-')
end

# urls: Imperative version
def imperative_urls(states)
  urls = []
  states.each do |state|
    urls << urlify(state)
  end
  urls
end

puts imperative_urls(states).inspect

# urls: Functional version
def functional_urls(states)
  states.collect { |state| urlify(state) }
end
puts functional_urls(states).inspect

def create_urls(states)
	states.map { |state| my_urlify(state) }
end

puts create_urls(states).inspect

#select operator

def imperative_singles(states)
  singles = []
  states.each do |state|
    if (state.split(/\s+/).length == 1)
      singles << state
    end
  end
  singles
end

puts imperative_singles(states).inspect

# singles: Functional version
def functional_singles(states)
  states.select { |state| state.split.length == 1 }
end

def functional_singles_two(states)
	states.reject do |state|
		state.split.length != 1
	end
end

puts functional_singles(states).inspect
puts functional_singles_two(states).inspect

#excercises
def dakota_text(states)
	states.select { |state| state.downcase.include?("dakota") }
end

puts dakota_text(states).inspect

def functional_doubles(states)
	states.select { |state| /\w+\s+\w+/.match(state) }
end

puts functional_doubles(states).inspect

numbers = 1..10

# sum: Imperative solution
def imperative_sum(numbers)
  total = 0
  numbers.each do |n|
    total += n
  end
  total
end
puts imperative_sum(numbers)

# sum: Functional solution
def functional_sum(numbers)
  numbers.reduce { |total, n| total += n }
end
puts functional_sum(numbers)

# lengths: Imperative version
def imperative_lengths(states)
  lengths = {}
  states.each do |state|
    lengths[state] = state.length
  end
  lengths
end
puts imperative_lengths(states)

# lengths: Functional version
def functional_lengths(states)
	states.inject({}) { |lengths, state| lengths[state] = state.length; lengths }
end
puts functional_lengths(states)

def functional_product(numbers)
	numbers.reduce { |total, n| total *= n }
end
puts functional_product(numbers)

