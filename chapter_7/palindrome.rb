class Phrase < String
	def processor(string)
		string.downcase
	end

	def processed_content
		processor(self)	
	end

	def palindrome?
		processed_content == processed_content.reverse
	end

	def louder
		upcase
	end
end

class TranslatedPhrase < Phrase
	attr_accessor :translation

	def initialize(content, translation)
		super(content)
		@translation = translation
	end

	def processed_content
		processor(translation)
	end
end

=begin
class String
	def palindrome?
		processed_content == processed_content.reverse
	end

	def blank?
		self.empty? || /\A\s+\z/.match?(self)
	end
	
	private

	def processed_content
		self.downcase
	end

end
=end

module Palindrome

	# Returns true for a palindrome, false otherwise.
	def palindrome?
		processed_content == processed_content.reverse
	end

	private

	# Returns content for palindrome testing.
	def processed_content
		self.to_s.downcase
	end
end


phrase = Phrase.new("Racecar")
puts phrase
phrase = "Able was I, ere I was Elba."
puts phrase
puts (1..100).max
puts (1..100).min
puts (1..100).sum




